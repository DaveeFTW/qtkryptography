#ifndef _SYMMETRIC_KEY_COUNTER_H_
#define _SYMMETRIC_KEY_COUNTER_H_

// qtkryptography
#include "QtKryptography_global.h"
#include "SymmetricKey.h"

// qt
#include <QObject>

class QTKRYPTOGRAPHY_SHARED_EXPORT SymmetricKeyCounter : public SymmetricKey
{
public:
    /*!
        @brief Construct key.
        @param bitlen The number of bits in the key.
    */
    SymmetricKeyCounter(void);

    /*!
        @brief Construct key.
        @param bitlen The number of bits in the key.
    */
    SymmetricKeyCounter(int bitlen);
    
    /*!
        @brief Construct key with QByteArray
        @details This function will construct the key object with the specified number of bits and set the key to the data contained within the passed QByteArray.
        @param bitlen The number of bits in the key.
        @param key A QByteArray containing the desired key values.
    */
    SymmetricKeyCounter(int bitlen, const QByteArray& key);
    
    /*!
        @brief construct key with hex string
        @details This function will construct the key object with the specified number of bits and set the key to the hexadecimal values specified in the hex string.
        @param bitlen The number of bits in the key.
        @param hex_str A string containing hexadecimal characters. Does not require prefix of "0x"
    */
    SymmetricKeyCounter(int bitlen, const char *hex_str);

    /*!
        @brief Construct key and copy settings from another key.
        @param key Key to copy values from.
    */
    SymmetricKeyCounter(const SymmetricKeyCounter& key);

    //! Destroys the SymmetricKeyCounter object
    virtual ~SymmetricKeyCounter(void);

    /*!
        @brief Assign a key to another.
        @param other The key to assign to this one.
        @return reference to this key for chaining.
    */
    SymmetricKeyCounter& operator=(const SymmetricKeyCounter& other);

    /*!
        @brief Increment the counter
        This function is the internal function used to increment the counter for CTR mode. It is called automatically by the CTR and does not require any user input.
    */
    void increment(void);
    
    /*!
        @brief Reset the counter
        This function is the internal function used to reset the counter for CTR mode. It is called automatically by the CTR and does not require any user input.
    */
    void reset(void);
    
protected:
    /*!
        @brief perform counter update for next block.
        @details This function is called at the end of each block of encryption/decryption. Re-implement this function for your own method of incrementing the counter. The default counter performs a big-endian increment.
        @param ctr The current count number of the object
        @param nonce The nonce that is set upon initialisation.
        @param last_ctr The last block generated. If first block, this is the nonce.
        @return the new block for CTR
    */
    virtual QByteArray doIncrement(quint64 ctr, const QByteArray& nonce, const QByteArray& last_ctr);

    /*!
        @brief reset the state of the counter.
        @details This function is called when a reset is requested. Re-implement this function if you require special reset functionality.
    */
    virtual void doReset(void);

private:
    class SymmetricKeyCounterPrivate;
    SymmetricKeyCounterPrivate *impl_;
};

#endif // _SYMMETRIC_KEY_COUNTER_H_
