#ifndef _HASH_H_
#define _HASH_H_

// qtkryptography
#include "QtKryptography_global.h"

// qt
#include <QString>
#include <QSharedPointer>

// forward declare our private class
class HashImpl;

class QTKRYPTOGRAPHY_SHARED_EXPORT Hash
{
public:
    /*!
        @brief Construct a hash algorithm by name
        @param alg The name of the hash algorithm to use. e.g, "SHA1, MD5, RIPEMD"
    */
    Hash(const QString& alg);

    /*!
        @brief Update the message to hash.
        @param data The data to add into the hash mathematics.
    */
    void update(const QByteArray& data);
    
    /*!
        @brief finialise the message hash and return the result. This will perform a reset on the object.
        @return a QByteArray containing the resultant hash.
    */
    QByteArray final(void);
    
    /*!
        @brief Reset the state of the hash calculation.
    */
    void reset(void);

    /*!
        @brief return the size of the block used in the cipher.
    */
    int blockSize(void) const;

    /*!
        @brief return the size of the digest produced
    */
    int digestSize(void) const;

    /*!
        @brief perform a check for invalid state.
        @return true if hash object is invalid.
    */
    bool operator!(void) const;

    /*!
        @brief perform a hash on data and return the result.
        @param alg The hashing algorithm to use.
        @param data The data to hash.
        @return a QByteArray containg the resultant hash.
    */
    static QByteArray hash(const QString& alg, const QByteArray& data);
    
    // DTOR
    ~Hash(void);

protected:
    //! Errors that can be returned by createHash
    enum CreationError
    {
        SUCCESS,            //!< Successful creation of object.
        UNKNOWN_SYMBOL      //!< The algorithm could not be found.
    };

    /*!
        @brief create the hash internally.
        @param alg See constructor
        @return 1 of the CreationError modes.
    */
    CreationError createHash(const QString& alg);
    
private:
    // define our implementation
    QSharedPointer<HashImpl> impl_;
};

#endif // _HASH_H_
