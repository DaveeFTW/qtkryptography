#ifndef _CIPHER_H_
#define _CIPHER_H_

// qtkryptography
#include "QtKryptography_global.h"
#include "SymmetricKey.h"

// qt
#include <QString>
#include <QSharedPointer>

// forward declare our private class
class CipherImpl;

class QTKRYPTOGRAPHY_SHARED_EXPORT Cipher
{
public:
    /*!
        @brief Construct a cipher algorithm by name and keys.
        @details Construct a cipher algorithm by name and given the appropriate keys. This will create an interface to a single cipher.
        @param alg The name of the cipher(s) to construct. e.g, "XOR" for xor cipher. "AES128" for AES128.
        @param key A SymmetricKey pointer apply to a SINGLE cipher. Note, that QtKryptography will delete the pointer before this function ends. aka, AFTER YOU CALL THIS FUNCTION KEY WILL POINT TO UNALLOCATED MEMORY.
        @note If multiple keys are required based on the alg input, the algorithm will be in an invalid state.
        @note If the key is unsuitable for the algorithm, the cipher will also be in an invalid state.
    */
    Cipher(const QString& alg, SymmetricKey* key);

    /*!
        @brief Construct a cipher algorithm by name and keys.
        @details Construct a cipher algorithm by name and given the appropriate keys. This will create an interface to a cipher (or sequence of ciphers) with filters applied automatically.
        @param alg The name of the cipher(s) to construct. e.g, "XOR" for xor cipher. "AES128" for AES128.
        @param keys A SymmetricKey::List of keys to apply to each of the ciphers/filters. Note, that QtKryptography will delete the pointers before this function ends. aka, AFTER YOU CALL THIS FUNCTION THE KEYS WILL POINT TO UNALLOCATED MEMORY.
        @note If any of the keys are unsuitable for the algorithm(s), the cipher will also be in an invalid state.
    */
    Cipher(const QString& alg, SymmetricKey::List &keys);

    /*!
        @brief perform an encryption in-place.
        @param data reference to a QByteArray to encrypt.
        @return A reference to the input.
    */
    QByteArray& encrypt(QByteArray& data);

    /*!
        @brief perform a decryption in-place.
        @param data reference to a QByteArray to decrypt.
        @return A reference to the input.
    */
    QByteArray& decrypt(QByteArray& data);

    /*!
        @brief perform a check for invalid state.
        @return true if cipher object is invalid.
    */
    bool operator!(void) const;

    // DTOR
    ~Cipher(void);

protected:

    //! Errors that can be returned by createCipher
    enum CreationError
    {
        SUCCESS,            //!< Successful creation of object.
        INSUFFICENT_KEYS,   //!< Insufficient keys have been supplied to fufil the algorithm's demands.
        UNKNOWN_SYMBOL,     //!< One of the cipher/filters provided could not be found.
        NO_FILTER_BASE,     //!< No base cipher was found to apply the filter to.
        MISSING_IV,         //!< An IV was not present in the key list for this filter.
        ERROR_ALG_KEY,      //!< The key supplied to the algorithm was not accepted by the algorithm. Ensure you're passing the correct number of bits.
        ERROR_BASE_CIPHER   //!< The base cipher is incompatible for this filter.
    };

    /*!
        @brief create the cipher internally.
        @param alg See constructor
        @param keys See constructor
        @return 1 of the CreationError modes.
    */
    CreationError createCipher(const QString& alg, SymmetricKey::List& keys);
    
private:
    // define our implementation
    typedef QList<CipherImpl *> CipherImplList;
    QSharedPointer<CipherImplList> impl_;
};

#endif // _CIPHER_H_
