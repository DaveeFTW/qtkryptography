#ifndef _MAC_H_
#define _MAC_H_

// qtkryptography
#include "QtKryptography_global.h"
#include "SymmetricKey.h"

// qt
#include <QString>
#include <QSharedPointer>

// forward declare our private class
class MACImpl;

class QTKRYPTOGRAPHY_SHARED_EXPORT MAC
{
public:
    /*!
        @brief Construct a MAC (Message Authentication Code) algorithm by name and key
        @param alg The name of the mac algorithm to use. e.g, "HMAC-SHA1, HMAC-MD5, CMAC-AES128 etc"
        @param key A SymmetricKey pointer apply to a SINGLE MAC. Note, that QtKryptography will delete the pointer before this function ends. aka, AFTER YOU CALL THIS FUNCTION KEY WILL POINT TO UNALLOCATED MEMORY.
        @note If multiple keys are required based on the alg input, the algorithm will be in an invalid state.
        @note If the key is unsuitable for the algorithm, the MAC will also be in an invalid state.
    */
    MAC(const QString& alg, SymmetricKey *key);

    /*!
        @brief Construct a MAC algorithm by name and keys.
        @details Construct a MAC algorithm by name and given the appropriate keys.
        @param alg The name of the MAC + other algorithm(s) relative to the MAC algorithm to construct. e.g, "CMAC-AES256-CTR"
        @param keys A SymmetricKey::List of keys to apply to each algorithm. Note, that QtKryptography will delete the pointers before this function ends. aka, AFTER YOU CALL THIS FUNCTION THE KEYS WILL POINT TO UNALLOCATED MEMORY.
        @note If any of the keys are unsuitable for the algorithm(s), the MAC will also be in an invalid state.
    */
    MAC(const QString& alg, SymmetricKey::List &keys);
    
    /*!
        @brief Update the message to MAC.
        @param data The data to add into the MAC mathematics.
    */
    void update(const QByteArray& data);
    
    /*!
        @brief finialise the message MAC and return the result. This will perform a reset on the object.
        @return a QByteArray containing the resultant MAC.
    */
    QByteArray final(void);
    
    /*!
        @brief Reset the state of the MAC calculation.
    */
    void reset(void);

    /*!
        @brief perform a check for invalid state.
        @return true if MAC object is invalid.
    */
    bool operator!(void) const;

    // DTOR
    ~MAC(void);

protected:
    //! Errors that can be returned by createMAC
    enum CreationError
    {
        SUCCESS,            //!< Successful creation of object.
        UNKNOWN_SYMBOL      //!< The algorithm could not be found.
    };

    /*!
        @brief create the mac internally.
        @param alg See constructor
        @param key See constructor
        @return 1 of the CreationError modes.
    */
    CreationError createMAC(const QString& alg, SymmetricKey::List& key);
    
private:
    // define our implementation
    QSharedPointer<MACImpl> impl_;
};

#endif // _MAC_H_
