#ifndef _SYMMETRIC_KEY_H_
#define _SYMMETRIC_KEY_H_

// qtkryptography
#include "QtKryptography_global.h"

// qt
#include <QList>

class QTKRYPTOGRAPHY_SHARED_EXPORT SymmetricKey
{
public:
    /*!
      @brief Construct key
    */
    SymmetricKey(void);

    /*!
        @brief Construct key.
        @param bitlen The number of bits in the key.
    */
    SymmetricKey(int bitlen);
    
    /*!
        @brief Construct key with QByteArray
        @details This function will construct the key object with the specified number of bits and set the key to the data contained within the passed QByteArray.
        @param bitlen The number of bits in the key.
        @param key A QByteArray containing the desired key values.
    */
    SymmetricKey(int bitlen, const QByteArray& key);
    
    /*!
        @brief construct key with hex string
        @details This function will construct the key object with the specified number of bits and set the key to the hexadecimal values specified in the hex string.
        @param bitlen The number of bits in the key.
        @param hex_str A string containing hexadecimal characters. Does not require prefix of "0x"
    */
    SymmetricKey(int bitlen, const char *hex_str);
    
    /*!
        @brief Construct key and copy settings from another key.
        @param key Key to copy values from.
    */
    SymmetricKey(const SymmetricKey& key);

    //! Destroys the SymmetricKey object
    virtual ~SymmetricKey(void);

    /*!
        @brief Get the number of bits in this key.
        @return the number of bits in the key.
    */
    int length(void) const;
    
    /*!
        @brief set the length of the key in bits.
        @details This functions takes an input of the number of number of bits to set the key to. If there is an already existing bit length specified, the key stored will be wiped and the value of the new key will be 0.
        @param bitlen The size of the key in bits.
    */
    void setBitLength(int bitlen);

    /*!
        @brief set the key from a QByteArray.
        @details This function takes a key input and will set the objects internal key to match. This function will also truncate or pad to fit the bit length of the object.
        @param key a QByteArray containing the desired key.
        @return the number of bits truncated from the key. For key that is smaller than the bit length, it will be padded using zero.
    */
    int set(const QByteArray& key);
    
    /*!
        @brief get byte at specified position.
        @details This function treats the key as a circular array and will return the byte as if it had moved n bytes around the circle.
        @param n The index to access a byte in the key.
        @return The byte held at this index.
    */
    quint8 getByte(int n);

    /*!
        @brief Get a copy of the key as a QByteArray.
        @details This function will return a QByteArray containing the key used in this object.
        @return A QByteArray representation of the key.
    */
    QByteArray data(void) const;

    /*!
        @brief Assign a key to another.
        @param other The key to assign to this one.
        @return reference to this key for chaining.
    */
    SymmetricKey& operator=(const SymmetricKey& other);
    
    //! A List of keys
    typedef QList<const SymmetricKey *> List;
    
private:
    class SymmetricKeyPrivate;
    SymmetricKeyPrivate *impl_;
};

#endif // _SYMMETRIC_KEY_H_
