// qtkryptography
#include "Endian.h"

namespace Endian
{
    bool isLittleEndian(void)
    {
        quint32 test = 1;
        quint8 *ptr = (quint8 *)(&test);

        // do a little test ;P
        return (ptr[0] == 1);
    }
}
