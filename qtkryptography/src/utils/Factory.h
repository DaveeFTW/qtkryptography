#ifndef _FACTORY_H_
#define _FACTORY_H_

// qt 
#include <QMap>

// std
#include <functional>

template <class T>
class Factory
{
public:
    // our beautiful function
    typedef std::function<T *()> CreateFunc;

    // our interface methods
    static void add(const QString& name, CreateFunc func)
    {
        // add to map
        FactoryMap& map = getMap();
        map.insert(name, func);
    }

    static T *create(const QString& name)
    {
        // get map
        FactoryMap& map = getMap();

        // create func
        return (exists(name) ? (map.value(name)()) : NULL);
    }

    static bool exists(const QString& name)
    {
        // get map
        FactoryMap& map = getMap();

        // check if it exists
        return map.contains(name);
    }

private:
    // typedef for usage ease
    typedef QMap<QString, CreateFunc> FactoryMap;

    // get a QMap that maps names to functions.
    static FactoryMap& getMap(void)
    {
        // prevent out of order initialisation
        static FactoryMap map;
        return map;
    }
};

#endif // _FACTORY_H_
