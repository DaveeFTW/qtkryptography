#ifndef _ENDIAN_H_
#define _ENDIAN_H_

#include <QtGlobal>
#include <cstring>

namespace Endian
{  
    // runtime test if the system is little endian
    bool isLittleEndian(void);

    template<typename T>
    bool copyLittleEndian(quint8 *out, const T *in, quint32 size)
    {
        // check there is enough data
        if (size % sizeof(T))
        {
            // nope, error
            return false;
        }

        // if little endian, just do a standard copy
        if (isLittleEndian())
        {
            // do memcpy
            std::memcpy(out, (const quint8 *)(in), size);
        }
        else
        {
            // ok, do copy
            for (quint32 i = 0; i < size; i += sizeof(T))
            {
                // get the next element
                T data = in[i/sizeof(T)];

                // inner loop
                for (quint32 k = 0; k < sizeof(T); k++)
                {
                    // write output
                    out[i + k] = data & 0xFF;
                    data >>= 8;
                }
            }
        }

        // success
        return true;
    }

    template<typename T>
    void writeLittleEndian(quint8 *out, const T in)
    {
        // write as little endian
        copyLittleEndian<T>(out, &in, sizeof(T));
    }

    template<typename T>
    T readLittleEndian(const quint8 *in)
    {
        T var;

        // read as little endian
        copyLittleEndian<T>((quint8 *)&var, (const T *)in, sizeof(T));

        // return value
        return var;
    }

    template<typename T>
    bool copyBigEndian(quint8 *out, const T *in, quint32 size)
    {
        //	check there is enough data
        if (size % sizeof(T))
        {
            // nope, error
            return false;
        }

        // if little endian, just do a standard copy
        if (!isLittleEndian())
        {
            // do memcpy
            std::memcpy(out, (const quint8 *)(in), size);
        }
        else
        {
            // ok, do copy
            for (quint32 i = 0; i < size; i += sizeof(T))
            {
                // get the next element
                T data = in[i/sizeof(T)];

                // inner loop
                for (quint32 k = 0; k < sizeof(T); k++)
                {
                    // write output in BE order
                    out[i + (sizeof(T) - (k + 1))] = data & 0xFF;
                    data >>= 8;
                }
            }
        }

        // success
        return true;
    }

    template<typename T>
    void writeBigEndian(quint8 *out, const T in)
    {
        // write as big endian
        copyBigEndian<T>(out, &in, sizeof(T));
    }

    template<typename T>
    T readBigEndian(const quint8 *in)
    {
        T var;

        // read as big endian
        copyBigEndian<T>((quint8 *)&var, (const T *)in, sizeof(T));

        // return value
        return var;
    }
}

#endif // _ENDIAN_H_
