// qtkryptography
#include "CBC.h"
#include "cipher/FilterFactory.h"

// qt
#include <QDebug>

// register filter
REGISTER_FILTER("CBC", CBC, FilterFactory::DEFAULT_IV);

QByteArray& CBC::encrypt(QByteArray& data)
{
    qDebug() << Q_FUNC_INFO << "doing encrypt.";

    // check if data size is aligned to block length
    if (data.size() % m_base->length())
    {
        qDebug() << Q_FUNC_INFO << "non-aligned data inputted!";
        
        /// @todo add in error checking
    }

    // set the cbc xor buffer to the IV
    QByteArray cbc_data(m_iv.data());
    QByteArray plain_block;

	// do encrypt
    for (int i = 0; i < data.size(); i += m_base->length())
    {
        // copy bytes
        plain_block = data.mid(i, m_base->length());
        
        // xor data
        for (int k = 0; k < m_base->length(); k++) plain_block[k] = plain_block[k] ^ cbc_data[k];

        // encrypt block
        m_base->encrypt(plain_block);
        
        // copy data
        cbc_data.fill(0);
        cbc_data = plain_block;
        data.replace(i, m_base->length(), plain_block);
    }

    // clear buffers
    cbc_data.clear();
    plain_block.clear();
    return data;
}

QByteArray& CBC::decrypt(QByteArray& data)
{
    // check if data size is aligned to block length
    if (data.size() % m_base->length())
    {
        qDebug() << Q_FUNC_INFO << "non-aligned data inputted!";
        
        /// @todo add in error checking
    }
    
    // set the cbc xor buffer to the IV
    QByteArray cbc_data(m_iv.data());
    QByteArray cipher_block;
    QByteArray prev_cipher_block;
    
	// do decrypt
    for (int i = 0; i < data.size(); i += m_base->length())
    {
        // copy block
        cipher_block = data.mid(i, m_base->length());
        
        qDebug() << "block:" << cbc_data.toHex();

        // copy block
        prev_cipher_block.fill(0);
        prev_cipher_block = cipher_block;
        
        // decrypt block
        m_base->decrypt(cipher_block);
        
        qDebug() << "dec block:" << cipher_block.toHex();

        // xor data
        for (int k = 0; k < m_base->length(); k++)
        {
            data[i+k] = (cipher_block[k] ^ cbc_data[k]);
        }

        // copy data
        cbc_data.fill(0);
        cbc_data = prev_cipher_block;
    }
    
    // clear buffers
    cbc_data.clear();
    cipher_block.clear();
    prev_cipher_block.clear();
    return data;
}

int CBC::length(void) const
{
    // forward to the implementation
    return m_base->length();
}

QString CBC::name(void) const
{
    // since we're a filter, return this name and base name
    QString name = m_base->name();
    return name.append("-CBC");
}

bool CBC::setKey(const SymmetricKey *iv)
{
    // check for default IV
    if (iv == NULL)
    {
        // set to all zero
        m_iv = SymmetricKey(m_base->length()*8);
    }
    else
    {
        // deref and copy
        m_iv = *iv;
    }

    // check we got a proper IV
    if (m_iv.length() != m_base->length()*8)
    {
        // error, incompatible bitlength
        qDebug() << Q_FUNC_INFO << "Block Cipher and IV has mismatching lengths!" << "IV:" << m_iv.length() << "Cipher:" << m_base->length()*8;
        return false;
    }

    // at this point, we're happy
    return true;
}

bool CBC::setSourceCipher(CipherImpl *source)
{
    // we need a block cipher! cast to one
    m_base = dynamic_cast<BlockCipher*>(source);

    // check if valid
    if (!m_base)
    {
        // not a block cipher!
        qDebug() << Q_FUNC_INFO << "Block Cipher is required for ECB. Got:" << source->name();
        return false;
    }

    // success
    return true;
}

CBC::CBC(void)
{
    qDebug() << Q_FUNC_INFO << "CTOR";
}

CBC::~CBC(void)
{
    qDebug() << Q_FUNC_INFO << "DTOR";
    
    // delete base cipher
    if (m_base)
        delete m_base;
}
