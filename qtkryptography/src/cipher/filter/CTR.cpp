// qtkryptography
#include "CTR.h"
#include "cipher/FilterFactory.h"

// qt
#include <QDebug>

// register filter
REGISTER_FILTER("CTR", CTR, FilterFactory::REQUIRE_IV);

QByteArray& CTR::encrypt(QByteArray& data)
{
    QByteArray nonce;

    // loop through all data in blocks
    for (int i = 0; i < data.size(); i += m_base->length())
    {
        // copy data
        nonce = m_key.data();

        // encrypt block
        m_base->encrypt(nonce);

        // check the remaining size
        int remaining = data.size()-i;

        // check if we have too much for this block
        if (remaining > m_base->length()) remaining = m_base->length();

        // xor with plaintext
        for (int k = 0; k < remaining; k++) data[i+k] = data[i+k] ^ nonce[k];

        // clear
        nonce.fill(0);

        // increment
        m_key.increment();
    }

    // return the result
    return data;
}

QByteArray& CTR::decrypt(QByteArray& data)
{
    // decrypt is same as encrypt
    return encrypt(data);
}

QString CTR::name(void) const
{
    // since we're a filter, return this name and base name
    QString name = m_base->name();
    return name.append("-CTR");
}

bool CTR::setKey(const SymmetricKey *iv)
{
    // check if base is invalid
    if (iv == NULL)
    {
        // iv is required for CTR
        qDebug() << Q_FUNC_INFO << "IV is required for CTR mode";
    }

    // perform a static cast
    m_key = *static_cast<const SymmetricKeyCounter *>(iv);

    // check we got a proper IV
    if (m_key.length() != m_base->length()*8)
    {
        // error, incompatible bitlength
        qDebug() << Q_FUNC_INFO << "Block Cipher and IV has mismatching lengths!" << "IV:" << m_key.length() << "Cipher:" << m_base->length()*8;
        return false;
    }

    // at this point, we're happy
    return true;
}

bool CTR::setSourceCipher(CipherImpl *source)
{
    // we need a block cipher! cast to one
    m_base = dynamic_cast<BlockCipher*>(source);

    // check if valid
    if (!m_base)
    {
        // not a block cipher!
        qDebug() << Q_FUNC_INFO << "Block Cipher is required for CTR. Got:" << source->name();
        return false;
    }

    // success
    return true;
}

CTR::CTR(void)
{
    qDebug() << Q_FUNC_INFO << "CTOR";
}

CTR::~CTR(void)
{
    qDebug() << Q_FUNC_INFO << "DTOR";
    
    // delete base cipher
    if (m_base)
        delete m_base;
}
