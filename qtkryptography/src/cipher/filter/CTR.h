#ifndef _CTR_H_
#define _CTR_H_

// qtkryptography
#include "cipher/BlockCipher.h"
#include "cipher/Filter.h"
#include "SymmetricKeyCounter.h"

class CTR : public Filter
{
public:
    // CTOR
    CTR(void);
    
    // set key
    virtual bool setKey(const SymmetricKey *key);

    // set source
    virtual bool setSourceCipher(CipherImpl *source);

    // encrypt + decrypt operations
    virtual QByteArray& encrypt(QByteArray& data);
    virtual QByteArray& decrypt(QByteArray& data);

    // get the name
    virtual QString name(void) const;

    // destructor
    virtual ~CTR(void);

private:
    // implementation
    BlockCipher *m_base;
    SymmetricKeyCounter m_key;
};

#endif // _CTR_H_
