#ifndef _CBC_H_
#define _CBC_H_

// create block cipher
#include "cipher/BlockCipher.h"
#include "cipher/Filter.h"
#include "SymmetricKey.h"

class CBC : public Filter, public BlockCipher
{
public:
    // CTOR
    CBC(void);

    // function to set key
    virtual bool setKey(const SymmetricKey *key);

    // set the source cipher
    virtual bool setSourceCipher(CipherImpl *source);

    // encrypt + decrypt operations
    virtual QByteArray& encrypt(QByteArray& data);
    virtual QByteArray& decrypt(QByteArray& data);
    
    // get the block size
    virtual int length(void) const;

    // get the name
    virtual QString name(void) const;

    // destructor
    virtual ~CBC(void);

private:
    // implementation
    BlockCipher *m_base;
    SymmetricKey m_iv;
};

#endif // _CBC_H_
