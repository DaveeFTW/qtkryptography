#ifndef _ECB_H_
#define _ECB_H_

// qtkryptography
#include "cipher/BlockCipher.h"
#include "cipher/Filter.h"
#include "SymmetricKey.h"

class ECB : public Filter, public BlockCipher
{
public:
    // CTOR
    ECB(void);

    // function to set key
    virtual bool setKey(const SymmetricKey *key);

    // set the source cipher
    virtual bool setSourceCipher(CipherImpl *source);

    // encrypt + decrypt operations
    virtual QByteArray& encrypt(QByteArray& data);
    virtual QByteArray& decrypt(QByteArray& data);
    
    // get the block size
    virtual int length(void) const;

    // get the name
    virtual QString name(void) const;

    // destructor
    virtual ~ECB(void);

private:
    // implementation
    BlockCipher *m_base;
};

#endif // _ECB_H_
