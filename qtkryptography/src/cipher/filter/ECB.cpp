// qtkryptography
#include "ECB.h"
#include "cipher/FilterFactory.h"

// qt
#include <QDebug>

// register filter
REGISTER_FILTER("ECB", ECB, FilterFactory::NO_IV);

QByteArray& ECB::encrypt(QByteArray& data)
{
    // check if data size is aligned to block length
    if (data.size() % m_base->length())
    {
        qDebug() << Q_FUNC_INFO << "non-aligned data inputted!";
        
        /// @todo add in error checking
    }

    // ECB mode does nothing special here
    return m_base->encrypt(data);
}

QByteArray& ECB::decrypt(QByteArray& data)
{
    // check if data size is aligned to block length
    if (data.size() % m_base->length())
    {
        qDebug() << Q_FUNC_INFO << "non-aligned data inputted!";
        
        /// @todo add in error checking
    }

    // ECB mode does nothing special here
    return m_base->decrypt(data);
}

int ECB::length(void) const
{
    // forward to the implementation
    return m_base->length();
}

QString ECB::name(void) const
{
    // since we're a filter, return this name and base name
    QString name = m_base->name();
    return name.append("-ECB");
}

bool ECB::setKey(const SymmetricKey *iv)
{
    // do nothing with IV
    Q_UNUSED(iv);

    // key is ok
    return true;
}

bool ECB::setSourceCipher(CipherImpl *source)
{
    // we need a block cipher! cast to one
    m_base = dynamic_cast<BlockCipher*>(source);

    // check if valid
    if (!m_base)
    {
        // not a block cipher!
        qDebug() << Q_FUNC_INFO << "Block Cipher is required for ECB. Got:" << source->name();
        return false;
    }

    // we're happy
    return true;
}

ECB::ECB(void)
{
    qDebug() << Q_FUNC_INFO << "CTOR";
}

ECB::~ECB(void)
{
    qDebug() << Q_FUNC_INFO << "DTOR";
    
    // delete base cipher
    if (m_base)
        delete m_base;
}
