#ifndef _CIPHER_FACTORY_H_
#define _CIPHER_FACTORY_H_

// qtkryptography
#include "utils/Factory.h"
#include "CipherImpl.h"

typedef Factory<CipherImpl> CipherFactory;

#define REGISTER_CIPHER(NAME, CLASSNAME)                    \
struct _##CLASSNAME##_constructor                           \
{                                                           \
    _##CLASSNAME##_constructor(void)                        \
    {                                                       \
        CipherFactory::add(NAME,                            \
        [](void)                                            \
        {                                                   \
            return new CLASSNAME;                           \
        });                                                 \
    }                                                       \
};                                                          \
static _##CLASSNAME##_constructor _##CLASSNAME##_cipherctor;

#endif // _CIPHER_FACTORY_H_
