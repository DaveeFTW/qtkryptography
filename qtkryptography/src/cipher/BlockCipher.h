#ifndef _BLOCK_CIPHER_H_
#define _BLOCK_CIPHER_H_

// qtkryptography
#include "CipherImpl.h"

class BlockCipher : public virtual CipherImpl
{
public:
    // encrypt/decrypt routines (defined for clarity)
    virtual QByteArray& encrypt(QByteArray& data) = 0;
    virtual QByteArray& decrypt(QByteArray& data) = 0;

    // get the block length in bytes
    virtual int length(void) const = 0;
    
    // dtor, virtual.
    virtual ~BlockCipher(void) { }
    
private:
};

#endif // _BLOCK_CIPHER_H_
