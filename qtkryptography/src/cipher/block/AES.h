#ifndef _AES_H_
#define _AES_H_

/*	$OpenBSD: rijndael.h,v 1.13 2008/06/09 07:49:45 djm Exp $ */

/**
 * rijndael-alg-fst.h
 *
 * @version 3.0 (December 2000)
 *
 * Optimised ANSI C code for the Rijndael cipher (now AES)
 *
 * @author Vincent Rijmen <vincent.rijmen@esat.kuleuven.ac.be>
 * @author Antoon Bosselaers <antoon.bosselaers@esat.kuleuven.ac.be>
 * @author Paulo Barreto <paulo.barreto@terra.com.br>
 *
 * This code is hereby placed in the public domain.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHORS ''AS IS'' AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHORS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

// qtkryptography
#include "cipher/BlockCipher.h"
#include "SymmetricKey.h"

class AES : public BlockCipher
{
public: 
    // set key function
    virtual bool setKey(const SymmetricKey *key);

    // encrypt + decrypt operations
    virtual QByteArray& encrypt(QByteArray& data);
    virtual QByteArray& decrypt(QByteArray& data);
    
    // get the block size
    virtual int length(void) const;

    // get the name
    virtual QString name(void) const;

    // destructor
    virtual ~AES(void) {}
    
protected:    
    // CTOR
    AES(int bitlen);
    
private:	
    // constants
    static const quint32 BLOCK_SIZE = 0x10;
    static const quint32 MAXROUNDS = 14;

    // number of bits
    int m_bits;

    // round and key
    quint32 m_Nr;
    quint32 m_dk[4 * (MAXROUNDS + 1)];
    quint32 m_ek[4 * (MAXROUNDS + 1)];

    void rijndaelDecrypt(const quint32 rk[], int Nr, const quint8 ct[BLOCK_SIZE], quint8 pt[BLOCK_SIZE]);
    void rijndaelEncrypt(const quint32 rk[], int Nr, const quint8 ct[BLOCK_SIZE], quint8 pt[BLOCK_SIZE]);
    int rijndaelKeySetupDec(quint32 rk[], const quint8 cipherKey[], int keyBits);
    int rijndaelKeySetupEnc(quint32 rk[], const quint8 cipherKey[], int keyBits);
    
    // our S-Box
    static const quint32 Te0[256];
    static const quint32 Te1[256];
    static const quint32 Te2[256];
    static const quint32 Te3[256];
    static const quint32 Te4[256];

    // inverse S-Box
    static const quint32 Td0[256];
    static const quint32 Td1[256];
    static const quint32 Td2[256];
    static const quint32 Td3[256];
    static const quint32 Td4[256];

    // RCON
    static const quint32 rcon[10];	
};

#endif /* _AES_H_ */
