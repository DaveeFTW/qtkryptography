// qtkryptography
#include "XOR.h"
#include "cipher/CipherFactory.h"

// qt
#include <QDebug>

// add to factory
REGISTER_CIPHER("XOR", XOR);

QByteArray& XOR::encrypt(QByteArray& data)
{
    // loop and crypt
    for (int i = 0; i < data.size(); i++)
    {
        // crypt
        data[i] = data[i] ^ m_key.getByte(i);
    }

    // return the encrypted (or decrypted) value
    return data;
}

QByteArray& XOR::decrypt(QByteArray& data)
{
    // encrypt/decrypt routines are the same
    return encrypt(data);
}

bool XOR::setKey(const SymmetricKey *key)
{
    // set the key
    m_key = *key;
    
    // check if bitlength is not equal than 0, if so return true
    return (key->length() != 0);
}

XOR::XOR(void)
{
    // debug
    qDebug() << Q_FUNC_INFO << "CTOR";
}
