#ifndef _XOR_H_
#define _XOR_H_

// qtkryptography
#include "cipher/CipherImpl.h"
#include "SymmetricKey.h"

class XOR : public CipherImpl
{
public:
    // CTOR
    XOR(void);
    
    // set key
    virtual bool setKey(const SymmetricKey *key);
    
    // encrypt/decrypt routines
    virtual QByteArray& encrypt(QByteArray& data);
    virtual QByteArray& decrypt(QByteArray& data);

    // get name
    virtual QString name(void) const { return "XOR"; }

private:
    // our key !
    SymmetricKey m_key;
};

#endif // _XOR_H_
