#ifndef _FILTER_FACTORY_H_
#define _FILTER_FACTORY_H_

// qtkryptography
#include "utils/Factory.h"
#include "Filter.h"

class FilterFactory : public Factory<Filter>
{
public:
    // filter IV modes
    enum IV
    {
        DEFAULT_IV = 0,
        REQUIRE_IV,
        NO_IV,
    };
    
    static void add(const QString& name, IV mode, CreateFunc func)
    {
        // add to map
        getModeMap().insert(name, mode);
        
        // add normally
        Factory<Filter>::add(name, func);
    }
    
    // get IV mode
    static IV getIvMode(const QString& name)
    {
        // return the value
        return getModeMap().value(name);
    }
    
private:
    // get a QMap that maps names to IV mode
    static QMap<QString, IV>& getModeMap(void)
    {
        // static declare to avoid any initialisation order problem
        static QMap<QString, IV> map;
        return map;
    }
};

#define REGISTER_FILTER(NAME, CLASSNAME, IV_MODE)           \
struct _##CLASSNAME##_constructor                           \
{                                                           \
    _##CLASSNAME##_constructor(void)                        \
    {                                                       \
        FilterFactory::add(NAME, IV_MODE,                   \
        [](void)                                            \
        {                                                   \
            return new CLASSNAME();                         \
        });                                                 \
    }                                                       \
};                                                          \
static _##CLASSNAME##_constructor _cipherctor;

#endif // _FILTER_FACTORY_H_
