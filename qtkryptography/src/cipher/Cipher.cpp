// qtkryptography
#include "Cipher.h"
#include "CipherImpl.h"
#include "CipherFactory.h"
#include "FilterFactory.h"

// qt
#include <QStringList>
#include <QScopedPointer>
#include <QDebug>

// anonoymous namespace
namespace
{
    struct ScopedPointerSymmetricKeyDeleterFromObject
    {
        static inline void cleanup(SymmetricKey::List *list)
        {
            qDeleteAll(*list);
        }
    };
}

QByteArray& Cipher::encrypt(QByteArray& data)
{
    // loop through everything
    for (int i = 0; i < impl_->size(); i++)
    {
        // encrypt
        data = impl_->at(i)->encrypt(data);
    }

    // return reference
    return data;
}

QByteArray& Cipher::decrypt(QByteArray& data)
{
    // loop through everything
    for (int i = 0; i < impl_->size(); i++)
    {
        // encrypt
        data = impl_->at(i)->decrypt(data);
    }

    // return reference
    return data;
}

bool Cipher::operator!(void) const
{
    return impl_.isNull();
}

Cipher::Cipher(const QString& alg, SymmetricKey *key)
    : impl_(QSharedPointer<CipherImplList>())
{
    qDebug() << Q_FUNC_INFO << "CTOR" << impl_.isNull();

    // create list
    SymmetricKey::List keys;
    keys.append(key);

    // create algorithm
    if (createCipher(alg, keys) != SUCCESS)
    {
        // failed to create algorithm
        qDebug() << Q_FUNC_INFO << "failed to create algorithm:" << alg;
        impl_ = QSharedPointer<CipherImplList>();
    }
}

Cipher::Cipher(const QString &alg, SymmetricKey::List& keys)
    : impl_(QSharedPointer<CipherImplList>())
{
    qDebug() << Q_FUNC_INFO << "CTOR";

    // create algorithm
    if (createCipher(alg, keys) != SUCCESS)
    {
        // failed to create algorithm
        qDebug() << Q_FUNC_INFO << "failed to create algorithm:" << alg;
        impl_ = QSharedPointer<CipherImplList>();
    }
}

Cipher::CreationError Cipher::createCipher(const QString& alg, SymmetricKey::List& key_list)
{
    qDebug() << Q_FUNC_INFO << "Creating algorithm:" << alg;

    // create scoped variable based on the the list
    QScopedPointer<SymmetricKey::List, ScopedPointerSymmetricKeyDeleterFromObject> keys(&key_list);

    // put algorithm into uppercase
    QString alg_ = alg.toUpper();

    // split string
    QStringList list = alg_.split("-", QString::SkipEmptyParts);

    // if no items, add string (e.g, "AES128" will have no items, so just pass that as the item)
    if (list.size() == 0)
    {
        // no items!
        list.append(alg_);
    }

    // set default, we dont have a base cipher
    CipherImpl *base = NULL;
    QSharedPointer<CipherImplList> impl(new CipherImplList);

    // do parse
    for (int i = 0, k = 0; i < list.size(); i++)
    {
        qDebug() << Q_FUNC_INFO << "building component:" << list.at(i);

        // check if this is a base cipher
        if (CipherFactory::exists(list.at(i)))
        {
            // check we have a key!
            if (k < keys->size())
            {
                // if there is a previous base, add it into this list
                if (base != NULL)
                {
                    // add it to list
                    impl->push_back(base);
                }

                // free key
                base = CipherFactory::create(list.at(i));

                // set key
                bool res = base->setKey(keys->at(k++));

                // check for error
                if (res == false)
                {
                    // error
                    qDebug() << Q_FUNC_INFO << "Error in key setup for algorithm:" << base->name();
                    delete base;
                    return ERROR_ALG_KEY;
                }
            }
            else
            {
                // no remaining keys
                qDebug() << Q_FUNC_INFO << "Insufficent keys for cipher:" << list.at(i);
                return INSUFFICENT_KEYS;
            }
        }

        // check if we have a filter
        else if (FilterFactory::exists(list.at(i)))
        {
            // Filter object
            Filter *filter;
            const SymmetricKey *key = NULL;

            // check if we have a base
            if (!base)
            {
                // no base cipher
                qDebug() << Q_FUNC_INFO << "Cannot apply filter without base cipher!";
                return NO_FILTER_BASE;
            }

            // get IV mode
            FilterFactory::IV iv_mode = FilterFactory::getIvMode(list.at(i));

            // check if there is a free key
            if (k < keys->size())
            {
                // does this filter require an IV?
                if (iv_mode != FilterFactory::NO_IV)
                {
                    // set key
                    key = keys->at(k++);
                }
            }

            // use a default key
            else if (iv_mode == FilterFactory::DEFAULT_IV)
            {
                // default key?
                //! @todo add default IV support?
            }

            // insufficient keys
            else if (iv_mode == FilterFactory::REQUIRE_IV)
            {
                qWarning() << Q_FUNC_INFO << "IV IS REQUIRED FOR FILTER:" << list.at(i);
                return NO_FILTER_BASE;
            }

            // get expected key
            filter = FilterFactory::create(list.at(i));

            // set source
            if (filter->setSourceCipher(base) == false)
            {
                // error
                qDebug() << Q_FUNC_INFO << "Error setting source cipher for:" << filter->name();
                delete filter;
                delete base;
                return ERROR_BASE_CIPHER;
            }

            // set key
            if (filter->setKey(key) == false)
            {
                // error
                qDebug() << Q_FUNC_INFO << "Error in key setup for algorithm:" << filter->name();
                delete filter;
                return ERROR_ALG_KEY;
            }

            // set base
            base = filter;
        }

        else
        {
            // unknown symbol
            qDebug() << Q_FUNC_INFO << "Unknown symbol:" << list.at(i);
            return UNKNOWN_SYMBOL;
        }
    }

    // add in base
    impl->push_back(base);
    impl_ = impl;
    return SUCCESS;
}

Cipher::~Cipher(void)
{
    qDebug() << Q_FUNC_INFO << "DTOR";

    // check if we have implementation
    if (!impl_.isNull())
    {
        // delete everything
        qDeleteAll(*impl_.data());
    }
}
