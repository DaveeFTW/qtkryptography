#ifndef _CIPHER_IMPL_H_
#define _CIPHER_IMPL_H_

// qt
#include <QString>
#include <QByteArray>

// forward declare as not to drag in more includes
class SymmetricKey;

class CipherImpl
{
public:
    // function to set key
    virtual bool setKey(const SymmetricKey *key) = 0;

    // encrypt + decrypt operations
    virtual QByteArray& encrypt(QByteArray& data) = 0;
    virtual QByteArray& decrypt(QByteArray& data) = 0;

    // get the name of the algorithm
    virtual QString name(void) const = 0;

    // virtual destructor
    virtual ~CipherImpl(void) {}
};

#endif // _CIPHER_IMPL_H_
