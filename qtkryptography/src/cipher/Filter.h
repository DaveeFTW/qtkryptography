#ifndef _FILTER_H_
#define _FILTER_H_

// qtkryptography
#include "CipherImpl.h"

class Filter : public virtual CipherImpl
{
public:
    // set the source cipher
    virtual bool setSourceCipher(CipherImpl *source) = 0;
    
private:
};

#endif // _FILTER_H_
