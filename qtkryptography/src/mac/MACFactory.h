#ifndef _MAC_FACTORY_H_
#define _MAC_FACTORY_H_

// qtkryptography
#include "utils/Factory.h"
#include "MACImpl.h"

typedef Factory<MACImpl> MACFactory;

#define REGISTER_MAC(NAME, CLASSNAME)                       \
struct _##CLASSNAME##_constructor                           \
{                                                           \
    _##CLASSNAME##_constructor(void)                        \
    {                                                       \
        MACFactory::add(NAME,                               \
        [](void)                                            \
        {                                                   \
            return new CLASSNAME;                           \
        });                                                 \
    }                                                       \
};                                                          \
static _##CLASSNAME##_constructor _##CLASSNAME##_cipherctor;

#endif // _MAC_FACTORY_H_
