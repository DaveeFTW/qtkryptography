#ifndef _HMAC_H_
#define _HMAC_H_

// qtkryptography
#include "MACImpl.h"
#include "SymmetricKey.h"

// qt
#include <QByteArray>
#include <QString>

// forward declare classes
class Hash;

class HMAC : public MACImpl
{
public:
    // CTOR
    HMAC(void);
    
    // update our hash
    virtual void update(const QByteArray& data);
    
    // get the hash digest
    virtual QByteArray final(void);
    
    // reset the hash state
    virtual void reset(void);

    // set the algorithm
    virtual bool setAlgorithm(const QString& alg, SymmetricKey::List& keys);
    
    // get the name
    virtual QString name(void) const;

    // DTOR
    virtual ~HMAC(void);
    
private:
    Hash *m_hash;
    SymmetricKey m_ipad;
    SymmetricKey m_opad;
};

#endif // _HMAC_H_
