#ifndef _MAC_IMPL_H_
#define _MAC_IMPL_H_

// qtkryptography
#include "SymmetricKey.h"

// qt
#include <QString>
#include <QByteArray>

class MACImpl
{
public:
    // update the hash context
    virtual void update(const QByteArray& data) = 0;

    // get the final digest
    virtual QByteArray final(void) = 0;

    // reset the hash context
    virtual void reset(void) = 0;

    // get the name of the algorithm
    virtual QString name(void) const = 0;

    // set the algorithm
    virtual bool setAlgorithm(const QString& alg, SymmetricKey::List& keys) = 0;
    
    // virtual destructor
    virtual ~MACImpl(void) {}
};

#endif // _MAC_IMPL_H_
