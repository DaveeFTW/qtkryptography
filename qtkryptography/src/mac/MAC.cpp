// qtkryptography
#include "MAC.h"
#include "MACImpl.h"
#include "MACFactory.h"

// qt
#include <QDebug>

bool MAC::operator!(void) const
{
    // return true if invalid impl
    return impl_.isNull();
}

void MAC::update(const QByteArray& data)
{
    // update mac
    impl_->update(data);
}

QByteArray MAC::final(void)
{
    // get digest
    return impl_->final();
}

void MAC::reset(void)
{
    // reset mac state
    return impl_->reset();
}

MAC::CreationError MAC::createMAC(const QString& alg, SymmetricKey::List& list)
{
    qDebug() << Q_FUNC_INFO << "Creating mac:" << alg;
    
    // convert everything to uppercase and get first section
    QString mac_name = alg.toUpper().section('-', 0, 0);
    
    // check if mac exists
    if (!MACFactory::exists(mac_name))
    {
        // no algorithm found
        qDebug() << Q_FUNC_INFO << "Cannot find mac algorithm!";
        return UNKNOWN_SYMBOL;
    }

    // create mac
    impl_ = QSharedPointer<MACImpl>(MACFactory::create(mac_name));
       
    // get the algorithm to set
    QString alg_name = alg.toUpper().section('-', 1, -1);

    // set it
    impl_->setAlgorithm(alg_name, list);

    // success!
    return SUCCESS;
}

MAC::MAC(const QString& alg, SymmetricKey *key)
{
    // debug
    qDebug() << Q_FUNC_INFO << "CTOR";

    // create list
    SymmetricKey::List keys;
    keys.append(key);
    
    // create object
    if (createMAC(alg, keys) != SUCCESS)
    {
        // error creating MAC
        qDebug() << Q_FUNC_INFO << "Error creating MAC object";
        impl_ = QSharedPointer<MACImpl>();
    }
}

MAC::MAC(const QString& alg, SymmetricKey::List& keys)
{
    // debug
    qDebug() << Q_FUNC_INFO << "CTOR";
    
    // create object
    if (createMAC(alg, keys) != SUCCESS)
    {
        // error creating MAC
        qDebug() << Q_FUNC_INFO << "Error creating MAC object";
        impl_ = QSharedPointer<MACImpl>();
    }
}

MAC::~MAC(void)
{
    // debug
    qDebug() << Q_FUNC_INFO << "DTOR";
}
