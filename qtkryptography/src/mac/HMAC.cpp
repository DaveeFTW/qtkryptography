// qtkryptography
#include "HMAC.h"
#include "MACFactory.h"
#include "Hash.h"

// qt
#include <QDebug>

// register with factory
REGISTER_MAC("HMAC", HMAC);

void HMAC::update(const QByteArray& data)
{
    // update hash
    m_hash->update(data);
}

QByteArray HMAC::final(void)
{
    // get hash
    QByteArray hash = m_hash->final();
    
    // create new hash
    m_hash->reset();

    // hash opad
    m_hash->update(m_opad.data());
    m_hash->update(hash);
    
    // get final hash
    return m_hash->final();
}

void HMAC::reset(void)
{
    // reset hash
    m_hash->reset();
    
    // update with ipad
    m_hash->update(m_ipad.data());
}

bool HMAC::setAlgorithm(const QString& alg, SymmetricKey::List& keys)
{
    // check keys
    if (keys.size() == 0)
    {
        // key is required
        qDebug() << Q_FUNC_INFO << "no keys passed";
        return false;
    }
    
    // copy key
    SymmetricKey key = *keys.at(0);

    // delete keys
    qDeleteAll(keys);
        
    // create algorithm
    m_hash = new Hash(alg);
    
    // check for error
    if (!m_hash)
    {   
        // error
        qDebug() << Q_FUNC_INFO << "error creating hash:" << alg;
        return false;
    }
    
    // truncate key if required
    if (key.length() > m_hash->blockSize()*8)
    {
        // truncate using hash algorithm
        m_hash->update(key.data());
        key = SymmetricKey(m_hash->digestSize()*8, m_hash->final());
        m_hash->reset();
    }
    
    // create ipad + opad
    QByteArray ipad(m_hash->blockSize(), 0x36);
    QByteArray opad(m_hash->blockSize(), 0x5C);
    
    // xor with key
    for (int i = 0; i < key.data().size(); i++)
    {
        // XOR ipad + opad
        ipad[i] = ipad[i] ^ key.getByte(i);
        opad[i] = opad[i] ^ key.getByte(i);
    }
    
    // set as SymmetricKeys
    m_ipad = SymmetricKey(m_hash->blockSize()*8, ipad);
    m_opad = SymmetricKey(m_hash->blockSize()*8, opad);
    
    // clear data
    ipad.fill(0);
    opad.fill(0);
    
    // success
    reset();
    return true;
}

QString HMAC::name(void) const
{
	// ok, we need to return algorithm name 
    return QString("HMAC-%1").arg("TODO");
}

HMAC::HMAC(void)
{
	// do nothing
}

HMAC::~HMAC(void)
{
    // if there is a hash, delete it
    if (m_hash)
        delete m_hash;
}
