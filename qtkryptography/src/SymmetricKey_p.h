#ifndef _SYMMETRIC_KEY_P_H_
#define _SYMMETRIC_KEY_P_H_

#include "SymmetricKey.h"

// qt
#include <QDebug>

////////////////////////////////////////////////////////////////////////////////////////
//
// Private Implementation
//
////////////////////////////////////////////////////////////////////////////////////////

/// \todo add destructor to clean up key.
namespace
{
    int inline bits2bytes(int bits)
    {
        // get the number of bytes required to store these bits
        return (bits/8)+((bits%8) ? (1) : (0));
    }
}

class SymmetricKey::SymmetricKeyPrivate
{
public:
    // constructor
    SymmetricKeyPrivate(void)
        : m_bitlen(0)
    {
        // debug
        qDebug() << Q_FUNC_INFO << "CTOR";
    }

    SymmetricKeyPrivate(int bitlen)
        : m_bitlen(bitlen),
          m_key(bits2bytes(bitlen), 0)
    {
        // debug
        qDebug() << Q_FUNC_INFO << "CTOR. Bitlength:" << bitlen;
    }

    // set bit length
    void setBitLength(int bitlen)
    {
        // resize + fill
        m_key.resize(bits2bytes(bitlen));
        m_key.fill(0);
        m_bitlen = bitlen;
    }

    // get bit length
    int length(void) const
    {
        // return bit length
        return m_bitlen;
    }
    
    // set the internal key
    int set(const QByteArray& key)
    {
        // set internal key
        m_key = key;
        qDebug() << "key" << key.toHex();

        // now pad if needed
        while (m_key.size() < bits2bytes(length())) m_key.append((char)0);
        
        // return number of bits over, or under
        return (key.size())-bits2bytes(length());
    }
    
    QByteArray data(void) const
    {
        // return a copy
        return m_key;
    }

    ~SymmetricKeyPrivate(void)
    {
        // clear buffer
        m_key.fill(0);
        m_key.clear();
    }

private:
    // bit length of the stored key
    int m_bitlen;
    QByteArray m_key;
};

#endif // _SYMMETRIC_KEY_P_H_
