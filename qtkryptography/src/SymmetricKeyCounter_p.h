#ifndef _SYMMETRIC_KEY_COUNTER_P_H_
#define _SYMMETRIC_KEY_COUNTER_P_H_

#include "SymmetricKeyCounter.h"

// qt
#include <QDebug>

////////////////////////////////////////////////////////////////////////////////////////
//
// Private Implementation
//
////////////////////////////////////////////////////////////////////////////////////////

/// \todo add destructor to clean up key.
namespace
{
    int inline bits2bytes(int bits)
    {
        // get the number of bytes required to store these bits
        return (bits/8)+((bits%8) ? (1) : (0));
    }
}

class SymmetricKeyCounter::SymmetricKeyCounterPrivate
{
public:
    // default constructor
    SymmetricKeyCounterPrivate(void)
        : m_ctr(0),
          m_bitlen(0)
    {
        // do nothing
        qDebug() << Q_FUNC_INFO << "CTOR";
    }

    // constructor
    SymmetricKeyCounterPrivate(int bitlen, const QByteArray& nonce) 
        : m_ctr(0),
          m_bitlen(bitlen)
    {
        // debug
        qDebug() << Q_FUNC_INFO << "CTOR. Bitlength:" << bitlen;
        
        // set nonce
        set(nonce);
    }

    // get bit length
    int length(void) const
    {
        // return bit length
        return m_bitlen;
    }
    
    quint64 counter(void)
    {
        // return the current counter
        return m_ctr;
    }
    
    void setCounter(quint64 ctr)
    {
        // set counter
        m_ctr = ctr;
    }
    
    // set the internal nonce
    int set(const QByteArray& nonce)
    {
        // set internal nonce
        m_nonce = nonce;
        qDebug() << "nonce" << m_nonce.toHex();

        // now pad if needed
        while (m_nonce.size() < bits2bytes(length())) m_nonce.append((char)0);
        
        // return number of bits over, or under
        return (m_nonce.size())-bits2bytes(length());
    }
    
    QByteArray nonce(void) const
    {
        // return a copy
        return m_nonce;
    }

    ~SymmetricKeyCounterPrivate(void)
    {
        // clear buffer
        m_nonce.fill(0);
        m_nonce.clear();
    }

private:
    // bit length of the stored key
    quint64 m_ctr;
    int m_bitlen;
    QByteArray m_nonce;
};

#endif // _SYMMETRIC_KEY_COUNTER_P_H_
