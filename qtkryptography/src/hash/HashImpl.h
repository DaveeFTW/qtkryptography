#ifndef _HASH_IMPL_H_
#define _HASH_IMPL_H_

// qt
#include <QString>
#include <QByteArray>

class HashImpl
{
public:
    // update the hash context
    virtual void update(const QByteArray& data) = 0;

    // get the final digest
    virtual QByteArray final(void) = 0;

    // reset the hash context
    virtual void reset(void) = 0;

    // get block size
    virtual int blockSize(void) const = 0;

    // get digest size
    virtual int digestSize(void) const = 0;

    // get the name of the algorithm
    virtual QString name(void) const = 0;

    // virtual destructor
    virtual ~HashImpl(void) {}
};

#endif // _HASH_IMPL_H_
