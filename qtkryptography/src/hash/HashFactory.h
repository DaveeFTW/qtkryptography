#ifndef _HASH_FACTORY_H_
#define _HASH_FACTORY_H_

// qtkryptography
#include "utils/Factory.h"
#include "HashImpl.h"

typedef Factory<HashImpl> HashFactory;

#define REGISTER_HASH(NAME, CLASSNAME)                      \
struct _##CLASSNAME##_constructor                           \
{                                                           \
    _##CLASSNAME##_constructor(void)                        \
    {                                                       \
        HashFactory::add(NAME,                              \
        [](void)                                            \
        {                                                   \
            return new CLASSNAME;                           \
        });                                                 \
    }                                                       \
};                                                          \
static _##CLASSNAME##_constructor _##CLASSNAME##_cipherctor;

#endif // _HASH_FACTORY_H_
