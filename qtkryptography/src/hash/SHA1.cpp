// qtkryptography
#include "SHA1.h"
#include "hash/HashFactory.h"
#include "utils/Endian.h"

REGISTER_HASH("SHA1", SHA1);

#define SHA1_TRANSFORM_1(b, c, d)	((b & c) | ((~b) & d))
#define SHA1_TRANSFORM_2(b, c, d)	((b ^ c) ^ d)
#define SHA1_TRANSFORM_3(b, c, d)	((b & c) | (b & d) | (c & d))
#define SHA1_TRANSFORM_4(b, c, d)	((b ^ c) ^ d)

#define ROTL(b, n) (((b) << (n)) | ((unsigned int)(b) >> (32 - (n))))

#define SHA1_TRANSFORM_CONSTANT_R2	(0x5A827999)
#define SHA1_TRANSFORM_CONSTANT_R3	(0x6ED9EBA1)
#define SHA1_TRANSFORM_CONSTANT_R5	(0x8F1BBCDC)
#define SHA1_TRANSFORM_CONSTANT_R7	(0xCA62C1D6)

#define SHA1_INIT_CONSTANT_K1		(0x67452301)
#define SHA1_INIT_CONSTANT_K2		(0xEFCDAB89)
#define SHA1_INIT_CONSTANT_K3    	(0x98BADCFE)
#define SHA1_INIT_CONSTANT_K4		(0x10325476)
#define SHA1_INIT_CONSTANT_K5		(0xC3D2E1F0)

void SHA1::transform(void)
{
	quint32 w[80];
	quint32 a, b, c, d, e, f, k;

	// break message chunk into 16 32 big endian words 
	for (int i = 0; i < 16; i++)
	{
		w[i] = ((block[i * 4] & 0xFF) << 24) |
        ((block[(i * 4) + 1] & 0xFF) << 16) |
        ((block[(i * 4) + 2] & 0xFF) << 8) |
        ((block[(i * 4) + 3] & 0xFF) << 0);
	}
	
	// extend the 32 bit words into 80 words 
	for (int i = 16; i < 80; i++)
	{
		// XOR previous data and left rotate 1 place 
		w[i] = ROTL(w[i - 3] ^ w[i - 8] ^ w[i - 14] ^ w[i - 16], 1);
	}

	// copy the current hash data 
	a = digest[0];
	b = digest[1];
	c = digest[2];
	d = digest[3];
	e = digest[4];
	
	// do a loop of 80 cycles for the avalanche effect 
	for (int i = 0; i < 80; i++)
	{
		// check which mode we're using 
		switch (i / 20)
		{
			// first transform type. 0 <= i <= 19 
			case 0:
			{
				
				// 2^30 * root 2 and transform 
				f = SHA1_TRANSFORM_1(b, c, d);
				k = SHA1_TRANSFORM_CONSTANT_R2;
				break;
			}
			
			// second transform type. 20 <= i <= 39 
			case 1:
			{
				// 2^30 * root 3 and transform 
				f = SHA1_TRANSFORM_2(b, c, d);
				k = SHA1_TRANSFORM_CONSTANT_R3;
				break;
			}
			
			// third transform type. 40 <= i <= 59 
			case 2:
			{
				// 2^30 * root 5 and transform 
				f = SHA1_TRANSFORM_3(b, c, d);
				k = SHA1_TRANSFORM_CONSTANT_R5;
				break;
			}
			
			// forth transform type. 60 <= i <= 79 
			case 3:
			default:
			{
				// 2^30 * root 7 and transform 
				f = SHA1_TRANSFORM_4(b, c, d);
				k = SHA1_TRANSFORM_CONSTANT_R7;
				break;
			}
		}
				
		// do the avalanche effect ! 
		quint32 temp = ROTL(a, 5) + f + e + k + w[i];
		e = d;
		d = c;
		c = ROTL(b, 30);
		b = a;
		a = temp;
	}
	
	// update our current hash 
	digest[0] += a;
	digest[1] += b;
	digest[2] += c;
	digest[3] += d;
	digest[4] += e;
	
	// set block index to zero 
	block_indx = 0;
}

QByteArray SHA1::final(void)
{
	// append bit 1 to the message and 7 0 bits 
	block[block_indx++] = 0x80;
	
	// if there is not sufficient space to stores the bitlength of the message then we must pad until there is 
    if (block_indx > (BLOCK_SIZE - 0x8))
	{
		// pad until the end of the current message 
        while (block_indx < (unsigned int)BLOCK_SIZE) block[block_indx++] = 0;
		
		// do a transform for this task 
		transform();	
	}
	
	// pad until we've got the congruency or in simpler terms, enough space 
    while (block_indx < (BLOCK_SIZE - 0x8)) block[block_indx++] = 0;

	// store the bit length in big-endian style 
    block[BLOCK_SIZE - 1] = (bit_len >> 0) & 0xFF;
    block[BLOCK_SIZE - 2] = (bit_len >> 8) & 0xFF;
    block[BLOCK_SIZE - 3] = (bit_len >> 16) & 0xFF;
    block[BLOCK_SIZE - 4] = (bit_len >> 24) & 0xFF;
    block[BLOCK_SIZE - 5] = (bit_len >> 32) & 0xFF;
    block[BLOCK_SIZE - 6] = (bit_len >> 40) & 0xFF;
    block[BLOCK_SIZE - 7] = (bit_len >> 48) & 0xFF;
    block[BLOCK_SIZE - 8] = (bit_len >> 56) & 0xFF;
	
	// ok, we're all done ! final transform for the block 
	transform();
	
    // our out hash
    QByteArray out_hash(digestSize(), 0);
    
	// ok, if it's a little endian system, we need to byte swap 
    Endian::copyBigEndian(reinterpret_cast<quint8 *>(out_hash.data()), digest, sizeof(digest));

    // return the hash
    return out_hash;
}

void SHA1::update(const QByteArray& data)
{
	// loop through the data 
    for (int i = 0; i < data.size(); i++)
	{
		// copy data into the block 
		block[block_indx++] = data[i];
		
		// if we have a full 512 bit block 
        if (block_indx == (unsigned int)BLOCK_SIZE)
		{
			// transform this block using avalanche effect 
			transform();
		}	
	}
	
	// now we set the bitsize 
	bit_len += data.size() * 8;
}

void SHA1::reset(void)
{
	// set all data to 0 
	bit_len = 0;
	block_indx = 0;
	
	// now we set the message digest *nothing up my sleeve* constants 
	digest[0] = SHA1_INIT_CONSTANT_K1;
	digest[1] = SHA1_INIT_CONSTANT_K2;
	digest[2] = SHA1_INIT_CONSTANT_K3;
	digest[3] = SHA1_INIT_CONSTANT_K4;
	digest[4] = SHA1_INIT_CONSTANT_K5;
}

int SHA1::blockSize(void) const
{
	// return the block size 
	return BLOCK_SIZE;
}

int SHA1::digestSize(void) const
{
	// return the digest size 
	return DIGEST_SIZE;
}

QString SHA1::name(void) const
{
	// ok, we need to return algorithm name 
	return "SHA1";
}

SHA1::SHA1(void)
{
	// do the init proceedures 
	reset();
}
