#ifndef _SHA1_H_
#define _SHA1_H_

// qtkryptography
#include "HashImpl.h"

// qt
#include <QByteArray>
#include <QString>

class SHA1 : public HashImpl
{
	public:
		// constructor
		SHA1(void);
		
		// update our hash
        virtual void update(const QByteArray& data);
        
        // get the hash digest
		virtual QByteArray final(void);
        
        // reset the hash state
		virtual void reset(void);

		// get the block size
        virtual int blockSize(void) const;
        
        // get the digest size
        virtual int digestSize(void) const;
        
        // get the name
        virtual QString name(void) const;
		
	private:
        // constants
        static const int DIGEST_SIZE = (0x14);
        static const int BLOCK_SIZE =  (0x40);
        
		// data
		int condition;
        quint64 bit_len;
        quint32 block_indx;
		quint8 block[BLOCK_SIZE];
		quint32 digest[DIGEST_SIZE / sizeof(quint32)];
		
		// internal transform function
		void transform(void);
};

#endif // _SHA1_H_
