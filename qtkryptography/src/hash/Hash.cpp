// qtkryptography
#include "Hash.h"
#include "HashImpl.h"
#include "HashFactory.h"

// qt
#include <QDebug>

bool Hash::operator!(void) const
{
    // return true if invalid impl
    return impl_.isNull();
}

void Hash::update(const QByteArray& data)
{
    // update hash
    impl_->update(data);
}

QByteArray Hash::final(void)
{
    // get digest
    return impl_->final();
}

void Hash::reset(void)
{
    // reset hash state
    return impl_->reset();
}

int Hash::blockSize(void) const
{
    // return the block size
    return impl_->blockSize();
}

int Hash::digestSize(void) const
{
    // return the digest size
    return impl_->digestSize();
}

Hash::CreationError Hash::createHash(const QString& alg)
{
    qDebug() << Q_FUNC_INFO << "Creating hash:" << alg;
    
    // convert everything to uppercase
    QString hash_name = alg.toUpper();
    
    // check if hash exists
    if (!HashFactory::exists(hash_name))
    {
        // no algorithm found
        qDebug() << Q_FUNC_INFO << "Cannot find hash algorithm!";
        return UNKNOWN_SYMBOL;
    }
    
    // create hash
    impl_ = QSharedPointer<HashImpl>(HashFactory::create(alg));
    
    // success!
    return SUCCESS;
}

Hash::Hash(const QString& alg)
{
    // debug
    qDebug() << Q_FUNC_INFO << "CTOR";
    
    // create object
    if (createHash(alg) != SUCCESS)
    {
        // error creating hash
        qDebug() << Q_FUNC_INFO << "Error creating hash object";
        impl_ = QSharedPointer<HashImpl>();
    }
}

Hash::~Hash(void)
{
    // debug
    qDebug() << Q_FUNC_INFO << "DTOR";
}
