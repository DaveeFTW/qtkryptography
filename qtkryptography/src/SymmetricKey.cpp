// qtkryptography
#include "SymmetricKey.h"
#include "SymmetricKey_p.h"

// qt
#include <QDebug>

////////////////////////////////////////////////////////////////////////////////////////
//
// Public Interface
//
////////////////////////////////////////////////////////////////////////////////////////

quint8 SymmetricKey::getByte(int n)
{
    // set the byte to 0 initially
    quint8 byte = 0;

    // get the data
    QByteArray data = impl_->data();

    // check length
    if (impl_->length() % 8)
    {
        // calculate where we would read from
        int pos = (n*8) % impl_->length();

        // we need 8 bits :)
        for (int i = 0; i < 8; i++)
        {
            // get bit and byte access info
            int tpos = (pos+i) % impl_->length();
            int bit = (tpos)%8;
            int access_byte = (tpos)/8 % data.size();

            // add bits
            byte |= ((data[access_byte] & (1 << (8 - bit - 1)))) >> (i - bit);
        }
    }
    else
    {
        // quick access
        byte = data[n%data.size()];
    }

    // debug output
    qDebug() << Q_FUNC_INFO << "Read byte:" << byte;
    return byte;
}

QByteArray SymmetricKey::data(void) const
{
    // get the key
    return impl_->data();
}

int SymmetricKey::set(const QByteArray& key)
{
    // set the key
    return impl_->set(key);
}

int SymmetricKey::length(void) const
{
    // return the bit length
    return impl_->length();
}

void SymmetricKey::setBitLength(int bitlen)
{
    // set the bitlength
    impl_->setBitLength(bitlen);
}

SymmetricKey& SymmetricKey::operator=(const SymmetricKey& other)
{
    // check for self assignment
    if (this == &other)
        return *this;

    // delete our implementation
    delete impl_;

    // create new object
    impl_ = new SymmetricKeyPrivate(other.length());
    impl_->set(other.data());

    qDebug() << "return";

    // return this reference
    return *this;
}

SymmetricKey::SymmetricKey(const SymmetricKey& key)
{
    // debug information
    qDebug() << Q_FUNC_INFO << "CTOR";

    // create new object
    impl_ = new SymmetricKeyPrivate(key.length());
    impl_->set(key.data());
}

SymmetricKey::SymmetricKey(int bitlen)
    : impl_(new SymmetricKeyPrivate(bitlen))
{
    // debug information
    qDebug() << Q_FUNC_INFO << "CTOR";
}

SymmetricKey::SymmetricKey(int bitlen, const QByteArray& key)
    : impl_(new SymmetricKeyPrivate(bitlen))
{
    // debug information
    qDebug() << Q_FUNC_INFO << "CTOR";
    
    // set the key
    impl_->set(key);
}

SymmetricKey::SymmetricKey(int bitlen, const char *hex_str)
    : impl_(new SymmetricKeyPrivate(bitlen))
{
    // debug information
    qDebug() << Q_FUNC_INFO << "CTOR";

    // set the key
    impl_->set(QByteArray::fromHex(hex_str));
}

SymmetricKey::SymmetricKey(void)
    : impl_(new SymmetricKeyPrivate())
{
    // debug information
    qDebug() << Q_FUNC_INFO << "CTOR";
}

SymmetricKey::~SymmetricKey(void)
{
    qDebug() << Q_FUNC_INFO << "DTOR";

    // delete implementation
    delete impl_;
}
