// qtkryptography
#include "SymmetricKeyCounter.h"
#include "SymmetricKeyCounter_p.h"

// qt
#include <QDebug>


////////////////////////////////////////////////////////////////////////////////////////
//
// Protected Interface
//
////////////////////////////////////////////////////////////////////////////////////////

QByteArray SymmetricKeyCounter::doIncrement(quint64 ctr, const QByteArray& nonce, const QByteArray& last_ctr)
{
    Q_UNUSED(nonce);
    Q_UNUSED(ctr);

    // get size of nonce/ctr
    int bytes = length()/8;

    // create our new ctr
    QByteArray new_ctr(last_ctr);

    // signal to add byte
    bool add_byte = true;

    // handle generically
    while (add_byte && bytes > 0)
    {
        // add new count
        new_ctr[bytes-1] = new_ctr[bytes-1] + 1;

        // set next flag. if overflow, update next byte
        add_byte = ((char)new_ctr[bytes-1] == 0);

        // subtract
        bytes--;
    }

    // return new ctr
    return new_ctr;
}

void SymmetricKeyCounter::doReset(void)
{
    // do nothing
}

////////////////////////////////////////////////////////////////////////////////////////
//
// Public Interface
//
////////////////////////////////////////////////////////////////////////////////////////

void SymmetricKeyCounter::increment(void)
{
    // get the current count and
    impl_->setCounter(impl_->counter()+1);
    set(doIncrement(impl_->counter(), impl_->nonce(), data()));
}

void SymmetricKeyCounter::reset(void)
{
    // reset the counter
    doReset();

    // set to nonce
    set(impl_->nonce());
    
    // set counter
    impl_->setCounter(0);
}

SymmetricKeyCounter& SymmetricKeyCounter::operator=(const SymmetricKeyCounter& other)
{
    // check for self assignment
    if (this == &other)
        return *this;

    // delete our implementation
    delete impl_;

    // explicitly copy
    SymmetricKey::operator=(other);

    // create new object
    impl_ = new SymmetricKeyCounterPrivate(other.length(), other.impl_->nonce());

    // return this reference
    return *this;
}

SymmetricKeyCounter::SymmetricKeyCounter(const SymmetricKeyCounter& key)
    : SymmetricKey(key)
{
    // debug information
    qDebug() << Q_FUNC_INFO << "CTOR";

    // create new object
    impl_ = new SymmetricKeyCounterPrivate(key.length(), key.impl_->nonce());
    impl_->setCounter(key.impl_->counter());
}

SymmetricKeyCounter::SymmetricKeyCounter(int bitlen, const QByteArray& key)
    : SymmetricKey(bitlen, key),
    impl_(new SymmetricKeyCounterPrivate(bitlen, key))
{
    // debug information
    qDebug() << Q_FUNC_INFO << "CTOR";
}

SymmetricKeyCounter::SymmetricKeyCounter(int bitlen, const char *hex_str)
    : SymmetricKey(bitlen, hex_str),
      impl_(new SymmetricKeyCounterPrivate(bitlen, QByteArray::fromHex(hex_str)))
{
    // debug information
    qDebug() << Q_FUNC_INFO << "CTOR";
}

SymmetricKeyCounter::SymmetricKeyCounter(void) :
    impl_(new SymmetricKeyCounterPrivate())
{
    // do nothing
    qDebug() << Q_FUNC_INFO << "CTOR";
}

SymmetricKeyCounter::~SymmetricKeyCounter(void)
{
    qDebug() << Q_FUNC_INFO << "DTOR";

    // delete implementation
    delete impl_;
}
