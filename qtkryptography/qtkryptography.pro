#-------------------------------------------------
#
# Project created by QtCreator 2012-09-01T15:52:30
#
#-------------------------------------------------

QT       -= gui

TARGET = QtKryptography
TEMPLATE = lib

CONFIG(release, debug|release):DEFINES += QT_NO_DEBUG_OUTPUT

DEFINES += QTKRYPTOGRAPHY_LIBRARY

INCLUDEPATH += include src

SOURCES += \
    src/SymmetricKey.cpp \
    src/cipher/Cipher.cpp \
    src/cipher/stream/XOR.cpp \
    src/cipher/block/AES.cpp \
    src/cipher/filter/ECB.cpp \
    src/cipher/filter/CBC.cpp \
    src/cipher/filter/CTR.cpp \
    src/SymmetricKeyCounter.cpp \
    src/hash/Hash.cpp \
    src/hash/SHA1.cpp \
    src/utils/Endian.cpp \
    src/mac/MAC.cpp \
    src/mac/HMAC.cpp

HEADERS += \
    include/QtKryptography_global.h \
    src/utils/Factory.h \
    include/SymmetricKey.h \
    src/SymmetricKey_p.h \
    include/Cipher.h \
    src/cipher/FilterFactory.h \
    src/cipher/CipherFactory.h \
    src/cipher/CipherFactory.h \
    src/cipher/Filter.h \
    src/cipher/CipherImpl.h \
    src/cipher/stream/XOR.h \
    src/cipher/block/AES.h \
    src/cipher/block/BlockCipher.h \
    src/cipher/BlockCipher.h \
    src/cipher/filter/ECB.h \
    src/cipher/filter/CBC.h \
    src/cipher/filter/CTR.h \
    include/SymmetricKeyCounter.h \
    src/SymmetricKeyCounter_p.h \
    src/hash/HashImpl.h \
    src/hash/HashFactory.h \
    src/hash/SHA1.h \
    src/utils/Endian.h \
    include/Hash.h \
    src/mac/MACImpl.h \
    src/mac/MAC.h \
    src/mac/MACFactory.h \
    include/MAC.h \
    src/mac/HMAC.h

libheaders.path = ../include
libheaders.files = include/*

debug {
    debugins.path = ../bin
    debugins.CONFIG = no_check_exist

    win32{
        debugins.files += debug/*.dll \
                          debug/*.lib
    }else:unix{
        debugins.files += *.so
    }else{
        debugins.files += debug/*
    }

    INSTALLS += debugins
}
else:release {
    releaseins.path = ../bin
    releaseins.CONFIG = no_check_exist

    win32{
        releaseins.files += release/*.dll \
                            release/*.lib
    }else:unix{
        releaseins.files += *.so
    }else{
        releaseins.files += release/*
    }

    INSTALLS += releaseins
}

INSTALLS += libheaders

message("Out=" + $$IN_PWD)

*-g++* {
        # GCC
        QMAKE_CXXFLAGS += -std=c++11
}

symbian {
    MMP_RULES += EXPORTUNFROZEN
    TARGET.UID3 = 0xE0820717
    TARGET.CAPABILITY =
    TARGET.EPOCALLOWDLLDATA = 1
    addFiles.sources = QtKryptography.dll
    addFiles.path = !:/sys/bin
    DEPLOYMENT += addFiles
}

unix:!symbian {
    maemo5 {
        target.path = /opt/usr/lib
    } else {
        linux-*-64 {
            target.path = /usr/lib64
        } else {
            target.path = /usr/lib
        }
    }
    INSTALLS += target
}
