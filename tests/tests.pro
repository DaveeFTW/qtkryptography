#-------------------------------------------------
#
# Project created by QtCreator 2012-08-13T22:44:30
#
#-------------------------------------------------

QT       += testlib
QT       -= gui

TARGET = QtKryptographyTest
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += \
    QtKryptographyTest.cpp


INCLUDEPATH += ../include
DEPENDPATH += ../include

win32: LIBS += -L../bin -lQtKryptography
else:symbian: LIBS += -lQtKryptography
else:unix: LIBS += -L../bin -lQtKryptography
