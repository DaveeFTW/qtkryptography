#include <QtCore/QString>
#include <QtTest/QtTest>

// qtkryptography
#include <SymmetricKey.h>
#include <SymmetricKeyCounter.h>
#include <Cipher.h>
#include <Hash.h>
#include <MAC.h>

class QtKryptographyTest : public QObject
{
    Q_OBJECT
    
public:
    // CTOR
    QtKryptographyTest(void) { }

private Q_SLOTS:
    // test for the symmetric keys
    void SymmetricKeyTest(void);

    // test for Cipher object construction
    void CipherTest(void);

    // test for AES
    void AES128(void);

    // test for AES192
    void AES192(void);

    // test for AES256
    void AES256(void);

    // test for CBC
    void CBCTest(void);

    // test for CTR
    void CTRTest(void);

    // test for SHA1
    void SHA1Test(void);

    // test for HMAC
    void HMACTest(void);
};

void QtKryptographyTest::HMACTest(void)
{
    MAC mac("HMAC-SHA1", new SymmetricKey(160, "0b0b0b0b0b0b0b0b0b0b0b0b0b0b0b0b0b0b0b0b"));
    mac.update("Hi There");

    // check for error
    QVERIFY2(mac.final().toHex() == "b617318655057264e28bc0b6fb378c8ef146be00", "Error with HMAC-SHA! Incorrect result.");
}

void QtKryptographyTest::SHA1Test(void)
{
    // create SHA1 object
    Hash sha1("SHA1");

    // check for error
    QVERIFY2(!!sha1, "Error, SHA1 object is invalid!");

    // do SHA1 test
    sha1.update("abc");
    QVERIFY2(sha1.final().toHex() == "a9993e364706816aba3e25717850c26c9cd0d89d", "Error SHA1 \"abc\" hash is incorrect");
}

void QtKryptographyTest::CTRTest(void)
{
    QList<QByteArray> test_data;

    // test 1
    test_data.append("6bc1bee22e409f96e93d7e117393172a");
    test_data.append("874d6191b620e3261bef6864990db6ce");

    // test 2
    test_data.append("ae2d8a571e03ac9c9eb76fac45af8e51");
    test_data.append("9806f66b7970fdff8617187bb9fffdff");

    // test 3
    test_data.append("30c81c46a35ce411e5fbc1191a0a52ef");
    test_data.append("5ae4df3edbd5d35e5b4f09020db03eab");

    // test 4
    test_data.append("f69f2445df4f9b17ad2b417be66c3710");
    test_data.append("1e031dda2fbe03d1792170a0f3009cee");


    SymmetricKey::List keys;
    keys.append(new SymmetricKey(128, "2b7e151628aed2a6abf7158809cf4f3c"));
    keys.append(new SymmetricKeyCounter(128, QByteArray::fromHex("f0f1f2f3f4f5f6f7f8f9fafbfcfdfeff")));

    // creat object
    Cipher aesctr("AES128-CTR", keys);

    // loop through data
    for (int i = 0; i < test_data.size(); i += 2)
    {
        // check if valid
        QVERIFY2(!!aesctr, "Failure, object is invalid!");

        // get data
        QByteArray data = QByteArray::fromHex(test_data[i]);

        // test encrypt
        QString enc = aesctr.encrypt(data).toHex();

        // test!
        QVERIFY2(enc == test_data[i+1], QString("Failure in AES128CTR encryption #%1").arg((i/2)+1).toAscii().constData());
    }
}

void QtKryptographyTest::CBCTest(void)
{
    QList<QByteArray> test_data;

    // test 1
    test_data.append("000102030405060708090A0B0C0D0E0F");
    test_data.append("6bc1bee22e409f96e93d7e117393172a");
    test_data.append("7649abac8119b246cee98e9b12e9197d");

    // test 2
    test_data.append("7649ABAC8119B246CEE98E9B12E9197D");
    test_data.append("ae2d8a571e03ac9c9eb76fac45af8e51");
    test_data.append("5086cb9b507219ee95db113a917678b2");

    // test 3
    test_data.append("5086CB9B507219EE95DB113A917678B2");
    test_data.append("30c81c46a35ce411e5fbc1191a0a52ef");
    test_data.append("73bed6b8e3c1743b7116e69e22229516");

    // test 4
    test_data.append("73BED6B8E3C1743B7116E69E22229516");
    test_data.append("f69f2445df4f9b17ad2b417be66c3710");
    test_data.append("3ff1caa1681fac09120eca307586e1a7");

    // loop through data
    for (int i = 0; i < test_data.size(); i += 3)
    {
        SymmetricKey::List keys;
        keys.append(new SymmetricKey(128, "2b7e151628aed2a6abf7158809cf4f3c"));
        keys.append(new SymmetricKey(128, QByteArray::fromHex(test_data[i])));

        // creat object
        Cipher aescbc("AES128-CBC", keys);

        // check if valid
        QVERIFY2(!!aescbc, "Failure, object is invalid!");

        // get data
        QByteArray data = QByteArray::fromHex(test_data[i+1]);

        // test encrypt
        QString enc = aescbc.encrypt(data).toHex();

        // test!
        QVERIFY2(enc == test_data[i+2], QString("Failure in AES128CBC encryption #%1").arg((i/3)+1).toAscii().constData());
    }
}

void QtKryptographyTest::AES256(void)
{
    // create AES key
    Cipher aes256("AES256", new SymmetricKey(256, "603deb1015ca71be2b73aef0857d77811f352c073b6108d72d9810a30914dff4"));

    // check if valid
    QVERIFY2(!!aes256, "Failure, object is invalid!");

    QList<QByteArray> test_data;

    // test 1
    test_data.append("6bc1bee22e409f96e93d7e117393172a");
    test_data.append("f3eed1bdb5d2a03c064b5a7e3db181f8");

    // test 2
    test_data.append("ae2d8a571e03ac9c9eb76fac45af8e51");
    test_data.append("591ccb10d410ed26dc5ba74a31362870");

    // test 3
    test_data.append("30c81c46a35ce411e5fbc1191a0a52ef");
    test_data.append("b6ed21b99ca6f4f9f153e7b1beafed1d");

    // test 4
    test_data.append("f69f2445df4f9b17ad2b417be66c3710");
    test_data.append("23304b7a39f9f3ff067d8d8f9e24ecc7");

    // loop through data
    for (int i = 0; i < test_data.size(); i += 2)
    {
        // get data
        QByteArray data = QByteArray::fromHex(test_data[i]);

        // test encrypt
        QString enc = aes256.encrypt(data).toHex();

        // test!
        QVERIFY2(enc == test_data[i+1], QString("Failure in AES256 encryption #%1").arg((i/2)+1).toAscii().constData());
    }
}

void QtKryptographyTest::AES192(void)
{
    // create AES key
    Cipher aes192("AES192", new SymmetricKey(192, "8e73b0f7da0e6452c810f32b809079e562f8ead2522c6b7b"));

    // check if valid
    QVERIFY2(!!aes192, "Failure, object is invalid!");

    QList<QByteArray> test_data;

    // test 1
    test_data.append("6bc1bee22e409f96e93d7e117393172a");
    test_data.append("bd334f1d6e45f25ff712a214571fa5cc");

    // test 2
    test_data.append("ae2d8a571e03ac9c9eb76fac45af8e51");
    test_data.append("974104846d0ad3ad7734ecb3ecee4eef");

    // test 3
    test_data.append("30c81c46a35ce411e5fbc1191a0a52ef");
    test_data.append("ef7afd2270e2e60adce0ba2face6444e");

    // test 4
    test_data.append("f69f2445df4f9b17ad2b417be66c3710");
    test_data.append("9a4b41ba738d6c72fb16691603c18e0e");

    // loop through data
    for (int i = 0; i < test_data.size(); i += 2)
    {
        // get data
        QByteArray data = QByteArray::fromHex(test_data[i]);

        // test encrypt
        QString enc = aes192.encrypt(data).toHex();

        // test!
        QVERIFY2(enc == test_data[i+1], QString("Failure in AES192 encryption #%1").arg((i/2)+1).toAscii().constData());
    }
}

void QtKryptographyTest::AES128(void)
{
    // create AES key
    Cipher aes128("AES128", new SymmetricKey(128, "2b7e151628aed2a6abf7158809cf4f3c"));

    // check if valid
    QVERIFY2(!!aes128, "Failure, object is invalid!");

    QList<QByteArray> test_data;

    // test 1
    test_data.append("6bc1bee22e409f96e93d7e117393172a");
    test_data.append("3ad77bb40d7a3660a89ecaf32466ef97");

    // test 2
    test_data.append("ae2d8a571e03ac9c9eb76fac45af8e51");
    test_data.append("f5d3d58503b9699de785895a96fdbaaf");

    // test 3
    test_data.append("30c81c46a35ce411e5fbc1191a0a52ef");
    test_data.append("43b1cd7f598ece23881b00e3ed030688");

    // test 4
    test_data.append("f69f2445df4f9b17ad2b417be66c3710");
    test_data.append("7b0c785e27e8ad3f8223207104725dd4");

    // loop through data
    for (int i = 0; i < test_data.size(); i += 2)
    {
        // get data
        QByteArray data = QByteArray::fromHex(test_data[i]);

        // test encrypt
        QString enc = aes128.encrypt(data).toHex();

        // test!
        QVERIFY2(enc == test_data[i+1], QString("Failure in AES128 encryption #%1").arg((i/2)+1).toAscii().constData());
    }
}

void QtKryptographyTest::CipherTest(void)
{
    SymmetricKey *key = new SymmetricKey(128);
    Cipher cipher("XOR", key);

    // check if valid object
    QVERIFY2(!!cipher, "Failure, object is invalid!");

    // check if invalid object
    key = new SymmetricKey(128);
    Cipher cipher_inv("", key);

    // check if valid object
    QVERIFY2(!cipher_inv, "Failure, object is valid! should be invalid");
}

void QtKryptographyTest::SymmetricKeyTest(void)
{
    // create objects
    SymmetricKey key(128);
    SymmetricKey weirdkey(129);

    // check we have valid key bit length
    QVERIFY2(key.length() == 128 && weirdkey.length() == 129, "Failure, invalid bit length returned!");

    // get key data
    QByteArray key1 = key.data();
    QByteArray key2 = weirdkey.data();

    // check sizes
    QVERIFY2(key1.size() == 0x10 && key2.size() == 0x11, "Failure, returns invalid default key size!");

    // set a big key
    QVERIFY2(key.set(QByteArray(0x11, 0)) > 0, "Failure, invalid result from large key set!");

    // set a little key
    QVERIFY2(key.set(QByteArray(0xF, 0)) < 0, "Failure, invalid result from small key set!");

    // set a correct size key
    QVERIFY2(key.set(QByteArray(0x10, 0)) == 0, "Failure, invalid result from matching key set!");

    // set a big key, with weird keysize
    QVERIFY2(weirdkey.set(QByteArray(0x12, 0)) > 0, "Failure, invalid result from large key set!");

    // set a little key
    QVERIFY2(weirdkey.set(QByteArray(0x10, 0)) < 0, "Failure, invalid result from small key set!");

    // set a correct size key
    QVERIFY2(weirdkey.set(QByteArray(0x11, 0)) == 0, "Failure, invalid result from matching key set!");

    unsigned char odd_keylength2[] =
    {
        0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x80
    };

    // set weird bitlength readings
    SymmetricKey oddkey(129);
    oddkey.set(QByteArray((char *)odd_keylength2, 0x11));
    quint8 val = oddkey.getByte(0x10);

    // assert value
    QVERIFY2(val == 192, "Failure, invalid continuous read from odd keysize");

    // read again
    QVERIFY2(oddkey.getByte(0x20) == 96, "Failure, invalid continous read from odd keysize");
}

QTEST_APPLESS_MAIN(QtKryptographyTest)

#include "QtKryptographyTest.moc"
