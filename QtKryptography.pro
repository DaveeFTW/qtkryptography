#-------------------------------------------------
#
# Project created by QtCreator 2012-09-01T15:52:30
#
#-------------------------------------------------

TARGET = QtKryptography
TEMPLATE = subdirs

build_docs.target = "doc"
build_docs.commands =	doxygen "docs/qtkryptography"
build_docs.depends =
QMAKE_EXTRA_TARGETS += build_docs

INCLUDEPATH = include

SUBDIRS += \
    qtkryptography \
    tests

tests.depends = qtkryptography
